﻿using System.Collections.Generic;
using ResortManagerAdmin.Domain;

namespace ResortManagerAdmin.Data.Repositories
{
	public interface IClientRepository
	{
		List<Client> GetAll();
	}
}