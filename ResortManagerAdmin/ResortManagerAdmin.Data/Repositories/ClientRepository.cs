﻿using ResortManagerAdmin.Common;
using ResortManagerAdmin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ResortManagerAdmin.Data.Repositories
{
	public class ClientRepository : IClientRepository
	{
		public List<Client> GetAll()
		{
			HttpStatusCode httpStatusCode;

			var clientsUri = new Uri("http://clientapi.resortdataservices.com/api/clients");

			var clients = Http.Get<List<Client>>(clientsUri, out httpStatusCode);

			return clients;
		}

		
	}
}
