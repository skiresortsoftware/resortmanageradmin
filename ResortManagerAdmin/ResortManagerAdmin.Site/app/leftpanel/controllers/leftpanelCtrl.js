﻿mainApp.controller('leftpanelCtrl', function ($scope, $http, $rootScope) {

	$scope.showEmployeeAdmin = function () {

		// $scope.employeesVisible = true;
		$rootScope.$broadcast('EMPLOYEE_ADMIN', true);
	};

	$scope.showEmployeeReports = function () {

		$rootScope.$broadcast('EMPLOYEE_REPORTS', true);
	};

	//var initialize = function () {
	//	$http.get(leagueApiConfig.getLeagues)
	//         .then(function (response) {
	//         	$scope.leagues = response.data;
	//         });
	//}();
});