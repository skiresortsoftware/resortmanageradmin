﻿loginApp.controller('loginCtrl', function ($scope, $http, $rootScope) {

	$scope.clients = {};

	var clientsUrl = 'http://clientapi.resortdataservices.com/api/clients';

	var initialize = function () {

		$http.get(clientsUrl)
            .then(function (response) {
            	$scope.clients = response.data;
            });
	}();
});