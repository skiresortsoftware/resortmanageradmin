﻿var securityRoutes = function () {

	return {
		getUser: 'http://securityv2.resortdataservices.com/api/users/{0}?clientToken={1}',
		getUsers: 'http://securityv2.resortdataservices.com/api/users?clientToken={0}',
		addUser: 'http://securityv2.resortdataservices.com/api/authentications/add?username={0}&password={1}&clientToken={2}'
	};
}();