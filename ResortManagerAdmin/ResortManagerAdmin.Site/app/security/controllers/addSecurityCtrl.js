﻿mainApp.controller('addSecurityCtrl', function ($scope, $http, $rootScope) {

	$scope.currentUser = {};

	var employeeId = 0;

	$scope.employee = {};

	$scope.user = {};

	$scope.initialize = function () {

		employeeId = getQuerystringNameValue('employeeId');

		$http.get(employeeRoutes.getemployee.replace('{0}', employeeId).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.employee = response.data;

             	var test = response.data;
             });
	}();

	$scope.saveSecurity = function (s) {

		$http.get(securityRoutes.addUser.replace('{0}', s.Username).replace('{1}', s.Password).replace('{2}', clientToken))
             .then(function (r) {

             	$scope.user = r.data;

             	var employee = $scope.employee;

             	employee.LoginId = r.data.UserId;

             	$.ajax(employeeRoutes.putemployee.replace('{0}', employeeId).replace('{1}', clientToken),
				{
					dataType: "json",
					type: "POST",
					data: employee,
					success: function (data) { console.log(data); },
					error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
				});

             });
	}
});