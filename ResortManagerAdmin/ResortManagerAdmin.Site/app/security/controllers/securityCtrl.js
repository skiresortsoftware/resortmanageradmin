﻿mainApp.controller('securityCtrl', function ($scope, $http, $rootScope) {

	$scope.currentUser = {};

	$scope.employee = {};

	var addUserAndUpdateEmployee = function (employeeId, username, password) {

		$http.get(securityRoutes.addUser.replace('{0}', username).replace('{1}',  password).replace('{2}', clientToken))
             .then(function (r) {

             	$scope.user = r.data;

             	var employee = $scope.employee;

             	employee.LoginId = r.data.UserId;

             	$.ajax(employeeRoutes.putemployee.replace('{0}', employeeId).replace('{1}', clientToken),
				{
					dataType: "json",
					type: "POST",
					data: employee,
					success: function (data) { console.log(data); },
					error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
				});

             });
	};

	$scope.initialize = function () {

		$scope.id = getQuerystringNameValue('id');
		$scope.employeeId = getQuerystringNameValue('employeeId');

		$http.get(employeeRoutes.getemployee.replace('{0}', $scope.employeeId).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.employee = response.data;
             });

		$http.get(securityRoutes.getUser.replace('{0}', $scope.id).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.currentUser = response.data;
             });
	}();

	$scope.saveSecurity = function (s) {

		if (_.has(s, 'NewUsername')) {
			addUserAndUpdateEmployee($scope.employee.Id, s.NewUsername, s.Password);
		}

		if (_.has(s, 'NewPassword')) {

		}

	};
});