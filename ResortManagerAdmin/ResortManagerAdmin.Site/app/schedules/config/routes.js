﻿var scheduleRoutes = function () {

	return {
		getseason: 'http://scheduleapi.resortdataservices.com/api/seasons/{0}?clientToken={1}',
		postseason: 'http://scheduleapi.resortdataservices.com/api/seasons?clientToken={0}',
		putseason: 'http://scheduleapi.resortdataservices.com/api/seasons/{0}?clientToken={1}&update=true',
		getseasons: 'http://scheduleapi.resortdataservices.com/api/seasons?clientToken={0}&current={1}',
		gettimes: 'http://scheduleapi.resortdataservices.com/api/shifttimes?clientToken={0}',
		gettime: 'http://scheduleapi.resortdataservices.com/api/shifttimes/{0}?clientToken={1}',
		gettypes: 'http://scheduleapi.resortdataservices.com/api/shifttypes?clientToken={0}',
		gettype: 'http://scheduleapi.resortdataservices.com/api/shifttypes/{0}?clientToken={1}',
		getschedules: 'http://scheduleapi.resortdataservices.com/api/schedules?clientToken={0}',
		getpriorities: 'http://scheduleapi.resortdataservices.com/api/priorities?clientToken={0}',
		getpriority: 'http://scheduleapi.resortdataservices.com/api/priorities/{0}?clientToken={1}',
		getshift: 'http://scheduleapi.resortdataservices.com/api/shifts/{0}?clientToken={1}',
		getshifts: 'http://scheduleapi.resortdataservices.com/api/shifts?clientToken={0}&seasonId={1}&from={2}&to={3}',
		getshiftaggregates: 'http://scheduleapi.resortdataservices.com/api/shifts/aggregates?clientToken={0}&seasonId={1}',
		postShift: 'http://scheduleapi.resortdataservices.com/api/shifts?clientToken={0}',
		deleteShift: 'http://scheduleapi.resortdataservices.com/api/shifts/{0}?clientToken={1}&delete=true',
		deleteShifts: 'http://scheduleapi.resortdataservices.com/api/shifts/delete?date={0}&seasonId={1}&typeId={2}&clientToken={3}&delete=true'
	};
}();