﻿mainApp.controller('schedulesCtrl', function ($scope, $http, $rootScope) {

	$scope.adminHeaderText = 'Schedule Administration';
	$scope.reportsHeaderText = 'Schedule Reports';

	$scope.schedules = {};
	$scope.scheduleCount = 0;

	$scope.seasons = {};
	$scope.seasonCount = 0;

	$scope.types = {};
	$scope.typeCount = 0;

	$scope.priorities = {};
	$scope.priorityCount = 0;

	$scope.times = {};
	$scope.timeCount = 0;

	$scope.initialize = function () {

		$http.get(scheduleRoutes.getseasons.replace('{0}', clientToken).replace('{1}', null))
             .then(function (response) {
             	$scope.seasons = response.data;
             	$scope.seasonCount = response.data.length;
             });

		$http.get(scheduleRoutes.getpriorities.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.priorities = response.data;
             	$scope.priorityCount = response.data.length;
             });

		$http.get(scheduleRoutes.gettypes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.types = response.data;
             	$scope.typeCount = response.data.length;
             });

		$http.get(scheduleRoutes.gettimes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.times = response.data;
             	$scope.timeCount = response.data.length;
             });


	}();
});