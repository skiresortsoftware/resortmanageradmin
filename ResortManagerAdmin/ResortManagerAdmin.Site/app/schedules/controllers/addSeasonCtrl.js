﻿mainApp.controller('addSeasonCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;

	$scope.initialize = function () {

		$scope.saveSeason = function (e) {

			var seasonToUpdate = e;

			$.ajax(scheduleRoutes.postseason.replace('{0}', clientToken),
			{
				dataType: "json",
				type: "POST",
				data: e,
				success: function (data) { console.log(data); },
				error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
			});

			setTimeout(function () {

			}, 2000);
		}
	}();
});