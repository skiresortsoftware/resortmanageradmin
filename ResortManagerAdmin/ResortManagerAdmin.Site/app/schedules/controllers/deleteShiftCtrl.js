﻿mainApp.controller('deleteShiftCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;
	$scope.time = {};
	$scope.type = {};
	
	$scope.priority = {};
	$scope.season = {};
	$scope.shift = {};

	$scope.initialize = function () {

		$scope.id = getQuerystringNameValue('id');

		$http.get(scheduleRoutes.getshift.replace('{0}', $scope.id).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.shift = response.data;

             	$http.get(scheduleRoutes.getseason.replace('{0}', response.data.SeasonId).replace('{1}', clientToken))
					 .then(function (response) {
             			$scope.season = response.data;
					 });

             	$http.get(scheduleRoutes.gettype.replace('{0}', response.data.ShiftTypeId).replace('{1}', clientToken))
					 .then(function (response) {
					 	$scope.type = response.data;
					 });

             	$http.get(scheduleRoutes.getpriority.replace('{0}', response.data.PriorityId).replace('{1}', clientToken))
					 .then(function (response) {
					 	$scope.priority = response.data;
					 });

             });

		
		
	}();

	$scope.deleteShift = function (s) {

		$.ajax(scheduleRoutes.deleteShift.replace('{0}', s.Id).replace('{1}', clientToken),
		{
			dataType: "json",
			type: "POST",
			data: s,
			success: function (data) { console.log(data); },
			error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
		});

		setTimeout(function () {

		}, 3000);
	}
});