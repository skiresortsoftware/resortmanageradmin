﻿mainApp.controller('shiftCtrl', function ($scope, $http, $rootScope) {

	$scope.currentPage = 0;
	$scope.currentSeason = {};
	$scope.showAddShift = false;
	$scope.currentSeason = {};
	$scope.id = 0;
	$scope.times = {};
	$scope.types = {};
	$scope.priorities = {};
	$scope.seasons = {};
	$scope.showEditShift = false;
	$scope.showHeader = true;
	$scope.currentShifts = {};

	$scope.safeApply = function (fn) {
		var phase = this.$root.$$phase;
		if (phase == '$apply' || phase == '$digest') {
			if (fn && (typeof (fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	$scope.toggleShowAddShift = function () {

		if ($scope.showAddShift)
			$scope.showAddShift = false;
		else
			$scope.showAddShift = true;
	};

	$scope.toggleShowEditShift = function () {

		$scope.safeApply(function () {
			if ($scope.showEditShift) {
				$scope.showEditShift = false;
				$scope.showHeader = true;
			}
			else {
				$scope.showEditShift = true;
				$scope.showHeader = false;
			}
		});
	};

	function timeAt(i) {
		var d = new Date(i),
			h = (d.getHours() < 10 ? '0' : '') + d.getHours(),
			m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
		return h + ':' + m;
	}

	function dateAt(i) {

		var dt = new Date(i);

		var y = dt.getYear();
		var m = dt.getMonth();
		var d = dt.getDay();

		return y + '-' + m + '-' + d;
	}

	$scope.deleteShifts = function (date, seasonId, typeId) {

		$.ajax(scheduleRoutes.deleteShifts.replace('{0}', date).replace('{1}', seasonId).replace('{2}', typeId).replace('{3}', clientToken),
		{
			dataType: "json",
			type: "POST",
			//data: s,
			success: function (data) {
				$http.get(scheduleRoutes.getshiftaggregates.replace('{0}', clientToken).replace('{1}', $scope.currentSeason.Id))
					 .then(function (response) {

			 			$scope.events = _.map(response.data, function (sa) {

			 				var e = {
			 					title: sa.TypeName + ' ' + timeAt(sa.Start) + '-' + timeAt(sa.End) + ' (' + sa.Total + ')',
			 					start: sa.Date,
			 					allDay: true,
			 					textColor: 'black'
			 				};

			 				return e;
			 			});

			 			$scope.currentShifts = _.filter(response.data, function (s) {

			 				var result = dateAt(s.Date) == dateAt($scope.selectedDate);

			 				return result;
			 			});

			 			$scope.toggleShowEditShift();

			 			$('#calendar').fullCalendar('removeEvents');
			 			$('#calendar').fullCalendar('addEventSource', $scope.events);
			 			$('#calendar').fullCalendar('rerenderEvents');
					 });
			},
			error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
		});

		setTimeout(function () {

		}, 3000);
	};


	$scope.saveShift = function (s) {

		var date = new Date(s.Date);
		var dateFormatted = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
		s.Date = dateFormatted;

		var timeId = s.TimeId;
		var time = _.find($scope.times, function (t) { return t.Id == timeId; });

		s.Start = time.Start;
		s.End = time.End;

		$.ajax(scheduleRoutes.postShift.replace('{0}', clientToken),
		{
			dataType: "json",
			type: "POST",
			data: s,
			success: function (data) {
				$http.get(scheduleRoutes.getshiftaggregates.replace('{0}', clientToken).replace('{1}', $scope.currentSeason.Id))
					 .then(function (response) {

					 	$scope.toggleShowAddShift();

					 	$scope.events = _.map(response.data, function (sa) {

					 		var e = {
					 			title: sa.TypeName + ' ' + timeAt(sa.Start) + '-' + timeAt(sa.End) + ' (' + sa.Total + ')',
					 			start: sa.Date,
					 			allDay: true,
					 			textColor: 'black'
					 		};

					 		return e;
					 	});

					 	$('#calendar').fullCalendar('removeEvents');
					 	$('#calendar').fullCalendar('addEventSource', $scope.events);
					 	$('#calendar').fullCalendar('rerenderEvents');
					 });
			},
			error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
		});

		setTimeout(function () {

		}, 3000);
	}

	$scope.initialize = function () {

		$http.get(scheduleRoutes.getseasons.replace('{0}', clientToken).replace('{1}', null))
             .then(function (response) {
             	$scope.seasons = response.data;
             });

		$http.get(scheduleRoutes.gettimes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.times = response.data;
             });

		$http.get(scheduleRoutes.gettypes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.types = response.data;
             });

		$http.get(scheduleRoutes.getpriorities.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.priorities = response.data;
             });

		$http.get(scheduleRoutes.getseasons.replace('{0}', clientToken).replace('{1}', true))
             .then(function (response) {
             	$scope.currentSeason = response.data[0];

             	$http.get(scheduleRoutes.getshiftaggregates.replace('{0}', clientToken).replace('{1}', $scope.currentSeason.Id))
					 .then(function (response) {

					 	$scope.events = _.map(response.data, function (sa) {

					 		var startTime = timeAt(sa.Start);
					 		var endTime = timeAt(sa.End);

					 		var e = {
					 			title: sa.TypeName + ' ' + startTime + '-' + endTime + ' (' + sa.Total + ')',
					 			start: sa.Date,
					 			allDay: true,
					 			textColor: 'black'
					 		};

					 		return e;
					 	});

					 	angular.element(document).ready(function () {
					 		$('#calendar').fullCalendar({
					 			events: $scope.events,
					 			windowResize: function (event, ui) {
					 				$('#calendar').fullCalendar('render');
					 			},
					 			eventClick: function (calEvent, jsEvent, view) {

					 				$scope.selectedDate = new Date(calEvent.start);
					 				$scope.currentShifts = _.filter(response.data, function (s) {

					 					var result = dateAt(s.Date) == dateAt($scope.selectedDate);

					 					return result;
					 				});

					 				$scope.toggleShowEditShift();

					 			}
					 		});
					 	});
					 });
				});
		}();
});