﻿mainApp.controller('addShiftCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;
	$scope.times = {};
	$scope.types = {};
	$scope.priorities = {};
	$scope.seasons = {};

	$scope.initialize = function () {

		$http.get(scheduleRoutes.getseasons.replace('{0}', clientToken).replace('{1}', null))
             .then(function (response) {
             	$scope.seasons = response.data;
             });

		$http.get(scheduleRoutes.gettimes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.times = response.data;
             });

		$http.get(scheduleRoutes.gettypes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.types = response.data;
             });

		$http.get(scheduleRoutes.getpriorities.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.priorities = response.data;
             });
		
	}();



	$scope.saveShift = function (s) {

		var date = new Date(s.Date);
		var dateFormatted = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
		s.Date = dateFormatted;

		var timeId = s.TimeId;
		var time = _.find($scope.times, function (t) { return t.Id == timeId; });

		s.Start = time.Start;
		s.End = time.End;

		$.ajax(scheduleRoutes.postShift.replace('{0}', clientToken),
		{
			dataType: "json",
			type: "POST",
			data: s,
			success: function (data) { console.log(data); },
			error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
		});

		setTimeout(function () {

		}, 3000);
	}
});