﻿mainApp.controller('editSeasonCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;

	$scope.initialize = function () {

		$scope.id = getQuerystringNameValue('id');

		var initialize = function () {

			$http.get(scheduleRoutes.getseason.replace('{0}', $scope.id).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.season = response.data;
             });
		}();

		$scope.saveSeason = function (e) {

			var employeeToUpdate = e;

			$.ajax(scheduleRoutes.putseason.replace('{0}', $scope.id).replace('{1}', clientToken),
			{
				dataType: "json",
				type: "POST",
				data: e,
				success: function (data) { console.log(data); },
				error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
			});

			setTimeout(function () {

			}, 2000);
		}
	}();
});