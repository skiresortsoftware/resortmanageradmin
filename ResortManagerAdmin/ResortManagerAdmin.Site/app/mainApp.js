﻿/// <reference path="security/views/security.html" />
/// <reference path="security/views/security.html" />
var mainApp = angular.module('mainApp', ['ngRoute', 'mgcrea.ngStrap']);

mainApp.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: '../../app/dashboard/views/index.html',
			controller: 'dashboardCtrl'
		}).
		// employee
		when('/employeeadmin', {
			templateUrl: '../../app/employees/views/admin.html',
			controller: 'employeesCtrl'
		}).
		when('/employees', {
			templateUrl: '../../app/employees/views/employees.html',
			controller: 'employeesCtrl'
		}).
		when('/addemployee', {
			templateUrl: '../../app/employees/views/addemployee.html',
			controller: 'addEmployeeCtrl'
		}).
		when('/editemployee', {
			templateUrl: '../../app/employees/views/editemployee.html',
			controller: 'editEmployeeCtrl'
		}).
		when('/deleteemployee', {
			templateUrl: '../../app/employees/views/deleteemployee.html',
			controller: 'deleteEmployeeCtrl'
		}).
		when('/employeereports', {
			templateUrl: '../../app/employees/views/reports.html',
			controller: 'employeesCtrl'
		}).
		when('/employeeschedules', {
			templateUrl: '../../app/employees/views/employeeschedules.html',
			controller: 'employeesCtrl'
		}).
		when('/employeesecurity', {
			templateUrl: '../../app/employees/views/employeesecurity.html',
			controller: 'employeesCtrl'
		}).
		// schedule
		when('/scheduleadmin', {
			templateUrl: '../../app/schedules/views/admin.html',
			controller: 'schedulesCtrl'
		}).
		when('/schedulereports', {
			templateUrl: '../../app/schedules/views/reports.html',
			controller: 'schedulesCtrl'
		}).
		when('/schedulequotos', {
			templateUrl: '../../app/schedules/views/quotos.html',
			controller: 'schedulesCtrl'
		}).
		when('/scheduleassigned', {
			templateUrl: '../../app/schedules/views/assignments.html',
			controller: 'schedulesCtrl'
		}).
		when('/shifts', {
			templateUrl: '../../app/schedules/views/shifts.html',
			controller: 'shiftCtrl'
		}).
		when('/addshift', {
			templateUrl: '../../app/schedules/views/addshift.html',
			controller: 'addShiftCtrl'
		}).
		when('/deleteshift', {
			templateUrl: '../../app/schedules/views/deleteshift.html',
			controller: 'deleteShiftCtrl'
		}).

		// season
		when('/addseason', {
			templateUrl: '../../app/schedules/views/addseason.html',
			controller: 'addSeasonCtrl'
		}).
		when('/editseason', {
			templateUrl: '../../app/schedules/views/editseason.html',
			controller: 'editSeasonCtrl'
		}).
		when('/seasons', {
			templateUrl: '../../app/schedules/views/seasons.html',
			controller: 'schedulesCtrl'
		}).

		// priority
		when('/addpriority', {
			templateUrl: '../../app/schedules/views/addpriority.html',
			controller: 'addPriorityCtrl'
		}).
		when('/editpriority', {
			templateUrl: '../../app/schedules/views/editpriority.html',
			controller: 'editPriorityCtrl'
		}).
		when('/schedulepriorities', {
			templateUrl: '../../app/schedules/views/schedulepriorities.html',
			controller: 'schedulesCtrl'
		}).

		// type
		when('/addtype', {
			templateUrl: '../../app/schedules/views/addtype.html',
			controller: 'addTypeCtrl'
		}).
		when('/edittype', {
			templateUrl: '../../app/schedules/views/edittype.html',
			controller: 'editTypeCtrl'
		}).
		when('/scheduletypes', {
			templateUrl: '../../app/schedules/views/scheduletypes.html',
			controller: 'schedulesCtrl'
		}).

		// times
		when('/addtimes', {
			templateUrl: '../../app/schedules/views/addtimes.html',
			controller: 'addTimeCtrl'
		}).
		when('/edittimes', {
			templateUrl: '../../app/schedules/views/edittimes.html',
			controller: 'editTimeCtrl'
		}).
		when('/scheduletimes', {
			templateUrl: '../../app/schedules/views/scheduletimes.html',
			controller: 'schedulesCtrl'
		}).

		// skischool
		when('/skischool', {
			templateUrl: '../../app/skischool/views/admin.html',
			controller: 'skischoolCtrl'
		}).

		// security
		when('/security', {
			templateUrl: '../../app/security/views/security.html',
			controller: 'securityCtrl'
		}).
		when('/addsecurity', {
			templateUrl: '../../app/security/views/addsecurity.html',
			controller: 'addSecurityCtrl'
		})
}]);

