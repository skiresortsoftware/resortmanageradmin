﻿var employeeRoutes = function () {

	return {
		getemployeetypes: 'http://employeeapi.resortdataservices.com/api/employeeTypes?clientToken={0}',
		getgenders: 'http://employeeapi.resortdataservices.com/api/genders?clientToken={0}',
		getemployeetitles: 'http://employeeapi.resortdataservices.com/api/employeeTitles?clientToken={0}',
		getemployee: 'http://employeeapi.resortdataservices.com/api/employees/{0}?clientToken={1}',
		putemployee: 'http://employeeapi.resortdataservices.com/api/employees/{0}?clientToken={1}',
		postemployee: 'http://employeeapi.resortdataservices.com/api/employees?clientToken={1}',
		getemployees: 'http://employeeapi.resortdataservices.com/api/employees?clientToken={0}',
		deleteemployee: 'http://employeeapi.resortdataservices.com/api/employees/{0}?clientToken={1}&delete=true'
	};
}();