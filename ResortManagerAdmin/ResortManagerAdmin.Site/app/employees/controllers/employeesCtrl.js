﻿mainApp.controller('employeesCtrl', function ($scope, $http, $rootScope) {

	$scope.adminHeaderText = 'Employee Administration';
	$scope.reportsHeaderText = 'Employee Reports';

	$scope.employees = {};
	$scope.employeeCount = 0;

	$scope.users = {};
	$scope.password;

	$scope.showPasswordUpdate = false;

	$scope.safeApply = function (fn) {
		var phase = this.$root.$$phase;
		if (phase == '$apply' || phase == '$digest') {
			if (fn && (typeof (fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	$scope.togglePasswordUpdate = function () {

		if ($scope.showPasswordUpdate)
			$scope.showPasswordUpdate = false;
		else
			$scope.showPasswordUpdate = true;
	};

	$scope.updatePasswords = function () {

	};

	$scope.initialize = function () {

		$http.get(employeeRoutes.getemployees.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.employees = response.data;
             	$scope.employeeCount = response.data.length;

             	var employees = $scope.employees;

             	$http.get(securityRoutes.getUsers.replace('{0}', clientToken))
					 .then(function (resp) {
             			$scope.users = resp.data;

             			

             			_.each(employees, function (e) {

             				var loginId = e.LoginId;

             				var user = _.filter($scope.users, function (u) {
             					return u.UserId == loginId;
             				});

             				e.User = user;
             			});

             			$scope.employees = employees;

             			var test = $scope.employees;
					 });
             });

		


	}();
});