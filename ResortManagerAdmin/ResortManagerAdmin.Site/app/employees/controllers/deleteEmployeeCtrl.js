﻿mainApp.controller('deleteEmployeeCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;

	$scope.employee = {};
	
	$scope.initialize = function () {

		$scope.id = getQuerystringNameValue('id');

		var initialize = function () {
			$http.get(employeeRoutes.getemployee.replace('{0}', $scope.id).replace('{1}', clientToken))
             .then(function (response) {
             	$scope.employee = response.data;
             });
		}();

		$scope.deleteEmployee = function (e) {

			$.ajax(employeeRoutes.deleteemployee.replace('{0}', $scope.id).replace('{1}', clientToken),
			{
				dataType: "json",
				type: "POST",
				data: e,
				success: function (data) { console.log(data); },
				error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
			});

			setTimeout(function () {
				
			}, 2000);
		}

		
	}();
});