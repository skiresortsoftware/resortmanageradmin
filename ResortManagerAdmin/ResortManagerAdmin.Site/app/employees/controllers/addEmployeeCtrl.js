﻿mainApp.controller('addEmployeeCtrl', function ($scope, $http, $rootScope) {

	$scope.id = 0;

	$scope.employee = {};
	
	$scope.initialize = function () {

		$scope.employeeTypes = {};
		$scope.genders = {};
		$scope.employeeTitles = {};

		var initialize = function () {

			$http.get(employeeRoutes.getemployeetypes.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.employeeTypes = response.data;
             });

			$http.get(employeeRoutes.getgenders.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.genders = response.data;
             });

			$http.get(employeeRoutes.getemployeetitles.replace('{0}', clientToken))
             .then(function (response) {
             	$scope.employeeTitles = response.data;
             });
		}();

		$scope.saveEmployee = function (e) {

			var date = new Date(e.Person.DateOfBirth);
			var dateFormatted = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
			e.Person.DateOfBirth = dateFormatted;

			$.ajax(employeeRoutes.postemployee.replace('{1}', clientToken),
			{
				dataType: "json",
				type: "POST",
				data: e,
				success: function (data) { console.log(data); },
				error: function (request, textStatus, errorThrown) { console.log("error " + textStatus + ": " + errorThrown); }
			});

			setTimeout(function () {

			}, 3000);
		}
	}();
});