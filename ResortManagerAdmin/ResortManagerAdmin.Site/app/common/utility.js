﻿function getQuerystringNameValue(name) {
	// For example... passing a name parameter of "name1" will return a value of "100", etc.
	// page.htm?name1=100&name2=101&name3=102

	var url = window.location.href;
	var start = url.indexOf('comment-') === -1 ? url.length : url.indexOf('comment-');
	var winURL = url.replace(url.substring(start, url.length), '');
	var queryStringArray = winURL.split("?");
	var queryStringParamArray = queryStringArray[1].split("&");
	var nameValue = null;

	for (var i = 0; i < queryStringParamArray.length; i++) {
		queryStringNameValueArray = queryStringParamArray[i].split("=");

		if (name == queryStringNameValueArray[0]) {
			nameValue = queryStringNameValueArray[1];
		}
	}

	return nameValue;
}