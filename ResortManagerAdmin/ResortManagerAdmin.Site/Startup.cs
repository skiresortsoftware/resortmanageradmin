﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ResortManagerAdmin.Site.Startup))]
namespace ResortManagerAdmin.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
